<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="{{ asset('js/app.js') }}" defer></script>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/" style="color:#F40505" >Examination</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" style="color:#60C236" href="/">Home <span class="sr-only">(current)</span></a>
      <!-- <a class="nav-item nav-link" href="#">Features</a> -->
      <!-- <a class="nav-item nav-link" href="#">Pricing</a> -->
      <!-- <a class="nav-item nav-link disabled" href="#">Disabled</a> -->
    </div>
  </div>
</nav>

<h1 class="text-center" style="color:#60C236">Questions</h1>
<hr>



<div class="container-fluid">
<div class="jumbotron jumbotron-fluid mt-2">
  <div class="container">
  <h3 class="text-center">Technical Questions</h3>
@foreach($technicalQuestions as $technicalQuestion)
<a href=" /api/view-question/{{$technicalQuestion->id}}">
    <p class=""> <b>  {{$technicalQuestion->question}} </b></p>
</a>

    <input type="radio" id="A" name="answer" value="A">
  <label for="A"> {{$technicalQuestion->A}}</label><br>
  <input type="radio" id="B" name="answer" value="B">
  <label for="A"> {{$technicalQuestion->B}}</label><br>
  <input type="radio" id="C" name="answer" value="C">
  <label for="A"> {{$technicalQuestion->C}}</label><br>
  <input type="radio" id="D" name="answer" value="D">
  <label for="A"> {{$technicalQuestion->D}}</label><br>
@endforeach
  </div>
</div>
{{ $technicalQuestions->links() }}




<div class="jumbotron jumbotron-fluid mt-2">
  <div class="container">
  <h3 class="text-center">Aptitude Questions</h3>
@foreach($aptitudeQuestions as $aptitudeQuestion)
<a href=" /api/view-question/{{$aptitudeQuestion->id}}">
    <p class=""> <b>    {{$aptitudeQuestion->question}} </b></p>
</a>

    <input type="radio" id="A" name="answer" value="A">
  <label for="A"> {{$aptitudeQuestion->A}}</label><br>
  <input type="radio" id="B" name="answer" value="B">
  <label for="A"> {{$aptitudeQuestion->B}}</label><br>
  <input type="radio" id="C" name="answer" value="C">
  <label for="A"> {{$aptitudeQuestion->C}}</label><br>
  <input type="radio" id="D" name="answer" value="D">
  <label for="A"> {{$aptitudeQuestion->D}}</label><br>
@endforeach
  </div>
</div>
{{ $aptitudeQuestions->links() }}




<div class="jumbotron jumbotron-fluid mt-2">
  <div class="container">
  <h3 class="text-center">Logical Questions</h3>
@foreach($logicalQuestions as $logicalQuestion)
<a href=" /api/view-question/{{$logicalQuestion->id}}">
    <p class=""> <b>  {{$logicalQuestion->question}} </b></p>
</a>

    <input type="radio" id="A" name="answer" value="A">
  <label for="A"> {{$logicalQuestion->A}}</label><br>
  <input type="radio" id="B" name="answer" value="B">
  <label for="A"> {{$logicalQuestion->B}}</label><br>
  <input type="radio" id="C" name="answer" value="C">
  <label for="A"> {{$logicalQuestion->C}}</label><br>
  <input type="radio" id="D" name="answer" value="D">
  <label for="A"> {{$logicalQuestion->D}}</label><br>
@endforeach
  </div>
</div>
{{ $logicalQuestions->links() }}

</div>
	<a href="javascript:history.go(-1)" class="btn float-right mr-5 btn-sm mt-3" title="Return to the previous page">&laquo; Go back</a>

















     
    
                                        
</body>
</html>