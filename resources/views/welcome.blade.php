<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="{{ asset('js/app.js') }}" defer></script>
    
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#" style="color:#F40505" >Examination</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <!-- <a class="nav-item nav-link active" style="color:#60C236" href="/all-posts">Blog <span class="sr-only">(current)</span></a> -->
      <!-- <a class="nav-item nav-link" href="#">Features</a> -->
      <!-- <a class="nav-item nav-link" href="#">Pricing</a> -->
      <!-- <a class="nav-item nav-link disabled" href="#">Disabled</a> -->
    </div>
  </div>
</nav>


<!-- <a href="/all-posts">Blog</a> -->
<div class="container">

<div class="jumbotron">
  <h1 class="display-4" style="color:#F40505"> Hello!</h1>
  <p class="lead">Welcome to examination.test,  <a href="/api/all-questions">clicking here</a> to view all questions.</p>
 
  <hr class="my-4">
  <h3 class= "text-center" style="color:#60C236">Create a new Question!!</h3>
  @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
 <p style="display:none; color:red" id="alert">Question created Sucessfully!!</p>
  @csrf
<div class="form-group">
    <label for="category"> <small> Category </small> </label>

    <select class="form-select form-select-sm form-control" id="category" name="category" aria-label=".form-select-sm example" required>
  <option selected>Open this select menu</option>
  <option value="Technical">Technical</option>
  <option value="Aptitude">Aptitude</option>
  <option value="Logical">Logical</option>
</select>
   
    <!-- <input type="text" class="form-control" id="" name="post_title" placeholder="" required> -->
  </div>


<div class="form-group">
    <label for="question">Question</label>
  <textarea class="form-control" autocomplete="off" id="question" rows="2" placeholder="Type a question" required></textarea>
</div>
<hr>
<p><b>**Options**</b></p>

  <div class="form-group">
    <label for="A">Option A</label>
    <input type="text" class="form-control" id="A" name="A" placeholder="Enter the first option" required>
  </div>

  <div class="form-group">
    <label for="B">Option B</label>
    <input type="text" class="form-control" id="B" name="B" placeholder="Enter the second option" required>
  </div>
  <div class="form-group">
    <label for="C">Option C</label>
    <input type="text" class="form-control" id="C" name="C" placeholder="Enter the third option" required>
  </div>
  <div class="form-group">
    <label for="D">Option D</label>
    <input type="text" class="form-control" id="D" name="D" placeholder="Enter the third option" required>
  </div>

  <hr>
<p><b>**Answer**</b></p>
<div class="form-group">
    <label for="answer">Answer</label>

    <select class="form-select form-select-sm form-control" name="answer" id="answer" aria-label=".form-select-sm example" required>
  <option selected>Open this select menu</option>
  <option value="A">A</option>
  <option value="B">B</option>
  <option value="C">C</option>
  <option value="D">D</option>
</select>
</div>

  
 <button  class="btn btn-primary" onclick="createQuestion()">Submit</button>


</div>

<!-- </form> -->
 
</div>

<script>
function createQuestion() {
var question = ('question', document.getElementById('question').value);
var category =('category', document.getElementById('category').value);
var answer =('answer', document.getElementById('answer').value);
var A =('A', document.getElementById('A').value);
var B =('B', document.getElementById('B').value);
var C =('C', document.getElementById('C').value);
var D =('D', document.getElementById('D').value);

var data = {
    "question":question ,
    "category": category,
    "answer": answer,
    "A": A,
    "B": B,
    "C": C,
    "D": D
}
  return fetch('/api/create-question', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  .then(function(response) {
    console.log(response)

    if (!response.ok) {
      throw new Error('Bad status code from server.');
    }

    return response.json();
  })
  .then(function(responseData) {
    console.log(responseData)

    if (!(responseData.data && responseData.data.success)) {
    document.getElementById("alert").style.display = "block";
    document.getElementById('question').value = '';
document.getElementById('category').value = '';
document.getElementById('answer').value = '';
document.getElementById('A').value = '';
document.getElementById('B').value = '';
document.getElementById('C').value= '';
document.getElementById('D').value = '';
    }
  });
}
</script>
</body>
</html>