<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Option;
use DB;



class QuestionController extends Controller
{
    public function create(Request $request)
    {
        request()->validate([
            'question' => 'required|unique:questions',
            'answer' => 'required',
            'category' => 'required',
            ]);
            $question = new Question;
            $question->question = $request->question;
            $question->category = $request->category;
            $question->answer = $request->answer;
            $question->save();
            $option = new Option([
                'A' => $request->A,
                'B' => $request->B,
                'C' => $request->C,
                'D' => $request->D,
            ]);
            $question->options()->save($option);
            return response()->json([
                      'success' => true
                    ]);

    }

    public function all(Request $request)
    {
        $aptitudeQuestions = DB::table('questions')
        ->where('category', '=', 'aptitude')
        ->join('options','questions.id','=','options.question_id')
        ->paginate(1, ['*'],'aptitudeQuestions');

        $logicalQuestions = DB::table('questions')
        ->where('category', '=', 'logical')
        ->join('options','questions.id','=','options.question_id')
        ->paginate(1, ['*'],'logicalQuestions');

        $technicalQuestions = DB::table('questions')
        ->where('category', '=', 'technical')
        ->join('options','questions.id','=','options.question_id')
        ->paginate(1, ['*'],'technicalQuestions');
        return view('allQuestions', compact('aptitudeQuestions','logicalQuestions','technicalQuestions'));
    }



    public function view(Request $request ,$id)
    {
        $question = DB::table('questions')
        ->where('id', '=', $id)
        ->first();
         $options = Option::where('question_id', $question->id)->first();
    //    dd($options);
        return view('viewQuestion', compact('question','options'));

        }

        public function edit(Request $request ,$id)
    {
        $question = DB::table('questions')
        ->where('id', '=', $id)
        ->first();
         $options = Option::where('question_id', $question->id)->first();
    //    dd($options);
        return view('editQuestion', compact('question','options'));

        }

        public function update(Request $request, $id)
        {
            request()->validate([
                'question' => 'required',
                'answer' => 'required',
                'category' => 'required',
                ]);
        $question = Question::find($id);
                $question->question = $request->question;
                $question->category = $request->category;
                $question->answer = $request->answer;
                $question->update();
         $options = Option::where('question_id', $question->id)->first();
                    $options->A= $request->A;
                    $options->B= $request->B;
                    $options->C= $request->C;
                    $options->D= $request->D;
                $options->update();
                return response()->json([
                          'success' => true
                        ]);
    
        }

        public function delete(Request $request,$id)
        {
        $question = Question::find($id);
        $question->delete();

            
            return view('welcome')->with('message','Question deleted sucessfully!!');

        }


    
}
