<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];

    public function Options()
    {
        return $this->hasOne(Question::class);
    }
}
