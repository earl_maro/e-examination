<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $guarded = [];
    // protected $fillable = ['A', 'B', 'C','D'];
    
     public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
