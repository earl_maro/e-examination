<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuestionController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('/create-question',function( Request $request){
//     return $request;
//     return response()->json([
//       'success' => true
//     ]);
//   });


Route::post('/create-question', [QuestionController::class, 'create']);

Route::get('/all-questions', [QuestionController::class, 'all']);

Route::get('/view-question/{id}', [QuestionController::class, 'view']);
Route::get('/edit-question/{id}', [QuestionController::class, 'edit']);
Route::post('/update-question/{id}', [QuestionController::class, 'update']);

Route::get('/delete-question/{id}', [QuestionController::class, 'delete']);



